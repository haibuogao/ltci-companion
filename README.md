Long Term Care Insurance + Robotics

Yes.  You heard it right!  Promoting the development of our human-like companions may reduce the cost of LTC insurance without putting tomorrow’s caregivers out of work.

Rolling Pals from Toyota and/or bipedal robots like Asimo from Honda are entertaining and skilled.  But they at this point in time are not as skilled or agile as licensed caregivers who take independent care of LTC patients.

Without going into technical details, our human-looking electromechanical caregivers must be agile, attentive, listen and respond accurately to voice commands.  Designs call for legs and arms, human-looking face, realistic eye movements, feet, soft hands and fingers, etc.  Robotic designs that make it through our future manufacturing facilities may not emit unnecessary mechanical sounds.  Design requirements outline them to be as quiet as a trained and trusted house pet you never see or hear until they know it’s silently time to wake you in the morning.

If one thinks about it, just how many patients on extended care do you think could survive daily visual contact with a  clicking or clanking robot contraption, getting them up and out of bed, making calls to their doctor or family, measuring vitals, or assisting them with using the bathroom (activities caregivers assist certain patients with)?

Have peace of mind in the knowledge that we desire to provide lifelike companions to assist our licensed caregivers so their clients may live in a normal caregiver setting in familiar surroundings.

Yes, our goal is to make companions available for free

Home-based robotic products that we test and make available online are not free. However, a portion of the profits from current and future sales are being used to help develop our companions. Companions we develop, test, and certify, are intended to be placed in homes for free for up to one year to humans over 60 who are already on long term care. Our companion’s monthly services are intended to be paid for by sponsors. Website visitors and our existing clients and prospects may soon feel good about being or becoming our customers and clients, knowing they can apply for our future companions when and if sponsors become available.*

A person over the age of 60 who is diagnosed by a medical doctor as not being able to do any 2 of the 6 activities of daily living are intended to become qualified candidates for our future home care robots. A family member or LTC client may, once trained companions become available, ask their licensed caregiver to apply for our future home care robots as a part of their in-home treatment. Otherwise the client may apply to lease one of our companions at a monthly fee based upon their income.

Before the client’s paid-for LTC program expires, the intention is for the client’s caregiver to apply for one of our sponsored companions. The companion will be tested by the licensed caregiver, certified, and qualified to care for the client for up to a maximum of 12 consecutive hours per day without the client’s human caregiver. For example, the companion does all care giving when the caregiver goes home, goes shopping, etc. When the caregiver returns to the client’s home the caregiver resumes supervision of the companion’s activities.

* A licensed caregiver is required in to supervise our sponsored caregivers. We’ll talk more about government and private sponsors in another blog.


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).